const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const concat = require('gulp-concat');
const gulpClean = require('gulp-clean');
const jsMin = require('gulp-js-minify')
const imgMin = require('gulp-imagemin');
const autoprefixer = require('gulp-autoprefixer');
const browserSync = require('browser-sync').create();
const uglify = require('gulp-uglify')
const cleanCSS = require('gulp-clean-css');

const processCss = () => {
    return gulp.src('src/scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('styles.min.css'))
        .pipe(autoprefixer({
            overrideBrowserslist: ['last 2 versions'],
            cascade:false}))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('dist/css/'))
        .pipe(browserSync.stream());
}

const processJs = () =>{
    return gulp.src('src/js/*.js')
        .pipe(concat('js.min.js'))
        .pipe(uglify())
        .pipe(jsMin())
        .pipe(gulp.dest('dist/js/'))
        .pipe(browserSync.stream());
}

const processImg = () => {
    return gulp.src('src/img/*')
        .pipe(imgMin())
        .pipe(gulp.dest('dist/img/'))

}
const clean = () => {
    return gulp.src('dist')
        .pipe(gulpClean())
}

const dev = () => {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });

    gulp.watch('src/scss/*.scss', processCss);
    gulp.watch('src/js/*.js', processJs);
    gulp.watch('./*.html', browserSync.reload);

}

gulp.task('clean', clean)
gulp.task('dev' , dev)
gulp.task('processCss', processCss);
gulp.task('processJs', processJs);
gulp.task('processImg', processImg)



gulp.task('build',
    gulp.series('clean',
        gulp.parallel(processCss,processJs,processImg)));