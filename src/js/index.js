let tab = function () {
    let headerBlock = document.querySelector('.list__block-image'),
        headerItem = document.querySelector('.list__item')

    headerBlock.addEventListener('click',()=>{
        if (headerBlock.classList.contains('is-active') && headerItem.classList.contains('is-active')){
            headerBlock.classList.remove('is-active');
            headerItem.classList.remove('is-active');
        }else {
            headerBlock.classList.add('is-active');
            headerItem.classList.add('is-active');
        }
    })
}
tab()